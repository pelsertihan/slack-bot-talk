from slack import WebClient
import slack
import time
import requests

# Imports the Google Cloud client library
from google.cloud import language
from google.cloud.language import enums
from google.cloud.language import types

import os

# Add environment path.
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "LabsLEDTalk.json"
print("GOOGLE_APPLICATION_CREDENTIALS: " + os.environ["GOOGLE_APPLICATION_CREDENTIALS"])

# Instantiates a client
client = language.LanguageServiceClient()

slack_token = ""  # <- Enter your 'Bot User OAuth Access Token" here
rtmclient = slack.RTMClient(token=slack_token)
slack_client = slack.WebClient(token=slack_token)

def get_trump_tweet():
    r = requests.get("https://api.tronalddump.io/random/quote")
    result = r.json()
    return result["value"]

def react_to_message(data, emoticon):
    print(
        slack_client.reactions_add(
            name=emoticon,
            timestamp=data["ts"],
            channel=data["channel"]
        )
    )

@slack.RTMClient.run_on(event='message')
def say_hello(**payload):
    data = payload['data']
    print(data)
    try:
        message_text = data["text"]
        document = types.Document(
            content=message_text, type=enums.Document.Type.PLAIN_TEXT
        )

        # Detects the sentiment of the text
        sentiment = client.analyze_sentiment(
            document=document
        ).document_sentiment
        print(
            "The sentiment of: \"" + str(message_text) + "\" \nis:\n" + str(sentiment)
        )
        if sentiment.score <= -0.5:
            react_to_message(data, "angry")
        elif sentiment.score > -0.5 and sentiment.score <= -0.2:
            react_to_message(data, "cry")
        elif sentiment.score > -0.2 and sentiment.score <= 0.1:
            react_to_message(data, "unamused")
        elif sentiment.score > 0.1 and sentiment.score <= 0.3:
            react_to_message(data, "neutral_face")
        elif sentiment.score > 0.3 and sentiment.score <= 0.5:
            react_to_message(data, "grin")
        elif sentiment.score > 0.5:
            react_to_message(data, "heart")
    except KeyError:
        pass

rtmclient.start()
