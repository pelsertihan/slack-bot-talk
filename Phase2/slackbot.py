from slack import WebClient
import slack
import time
import requests

slack_token = ""  # <- Enter your 'Bot User OAuth Access Token" here
rtmclient = slack.RTMClient(token=slack_token)

def get_trump_tweet():
    r = requests.get("https://api.tronalddump.io/random/quote")
    result = r.json()
    return result["value"]

@slack.RTMClient.run_on(event='message')
def say_hello(**payload):
    data = payload['data']
    print(data)
    try:
        if 'hello' in data['text'].lower():
            channel_id = data['channel']
            user = data['user']
            webclient = payload['web_client']
            webclient.chat_postMessage(
                channel=channel_id,
                text="Hi <@" + user +">!"
            )
        elif 'trump' in data['text'].lower():
            channel_id = data['channel']
            user = data['user']
            webclient = payload['web_client']
            webclient.chat_postMessage(
                channel=channel_id,
                text=get_trump_tweet()
            )
    except KeyError:
        pass

rtmclient.start()
